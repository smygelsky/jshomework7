function CreateList(array) {

    let ul = document.createElement('ul');
    document.body.append(ul);

    array.map(function (item) {

        let li = document.createElement('li');
        ul.append(li);
        return li.innerHTML = `array data ` + item;

    });
}

let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
CreateList(arr);

//DOM это программный интерфейс, позволяющий программам и скриптам получить доступ и редактировать HTML-, XHTML- и XML-документы.